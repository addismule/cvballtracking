### CV: Tennis Ball Tracking ###

This is my project for Computer Vision Spring 2017 class at GSU.


### Requiremnts ###

The project requires openCV version of at least 2.8 and later.

There are sample videos included in the project folder. Please provide substitute video files
if you intend to remove the ones provided. Two videos are required 'Top.mp4' which should be a
video capture of the entire Tennis table from the top view and 'Side.mp4' again a video that captures 
the entire Tennis table from an angle perpendicular to the length of the table. Note the files should 
be named accordingly.

### How to build and run? ###

Please run the following commands in the order they are listed below.
I have used CMake to build the project. So CMake is required if you are to build the project 
without having to write a MakeFile by yourself.


Navigate to the project directory.
> cd <path/to/project/folder>

Generate MakeFile.
> /Applications/CMake.app/Contents/bin/cmake .

Build the project
> make


Run the project executable
> ./motionTracking