#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <string>

using namespace std;
using namespace cv;
Mat src;
Mat src2;

Mat srcB1;
Mat srcB2;

const int FRAME_WIDTH = 640;
const int FRAME_HEIGHT = 480;


const int TABLE_HEIGHT_CENTIMETERS = 76;
const int TABLE_LENGTH_CENTIMETERS = 274;
const int TABLE_WIDTH_CENTIMETERS = 153;

const int MAX_OBJECT_AREA = FRAME_HEIGHT*FRAME_WIDTH/1.5;

const static int SENSITIVITY_VALUE = 20;
const static int BLUR_SIZE = 20;

string intToString(int number){

	std::stringstream ss;
	ss << number;
	return ss.str();
}

int main(){

	SimpleBlobDetector::Params params;

	// Change thresholds
	params.minThreshold = 10;
	params.maxThreshold = 200;

	// // Filter by Area.
	// params.filterByArea = true;
	// params.minArea = 1500;

	// Filter by Circularity
	params.filterByCircularity = true;
	params.minCircularity = 0.1;

	// // Filter by Convexity
	// params.filterByConvexity = true;
	// params.minConvexity = 0.87;



	// Storage for blobs
	vector<KeyPoint> keypointsA;
	vector<KeyPoint> keypointsB;

	Mat frame1;
	Mat frame2;

	Mat frameB1;
	Mat frameB2;

	Mat grayImage1;
	Mat grayImage2;

	Mat grayImageB1;
	Mat grayImageB2;

	Mat differenceImage;
	Mat thresholdImage;

	Mat differenceImageB;
	Mat thresholdImageB;

	Mat maskedThresholdImageB;
	Mat maskB;

	Mat im_with_keypointsA;
	Mat im_with_keypointsB;

	Mat cameraFeed;
	Mat cameraFeed2;

	VideoCapture capture;
	capture.open("Top.mp4");

	VideoCapture captureB;
	captureB.open("Side.mp4");

	int ballXPositonCM = 0;
	int ballYPositonCM = 0;
	int ballZPositonCM = 0;

	int tableTopPx;
	int tableHeightPx;
	int tableWidthPx;
	int tableLengthPx;



	waitKey(1000);
	while(1){
		//store image to matrix
		capture.read(frame1);
		capture.read(frame2);

		captureB.read(frameB1);
		captureB.read(frameB2);

		src = frame1;
		src2 = frame2;

		srcB1 = frameB1;
		srcB2 = frameB2;

		if( !src.data || !src2.data || !srcB1.data || !srcB2.data)
		{ return -1; }

		cv::cvtColor(frame1,grayImage1,COLOR_BGR2GRAY);
		cv::cvtColor(frame2,grayImage2,COLOR_BGR2GRAY);

		cv::cvtColor(frameB1,grayImageB1,COLOR_BGR2GRAY);
		cv::cvtColor(frameB2,grayImageB2,COLOR_BGR2GRAY);

		cv::absdiff(grayImage1,grayImage2,differenceImage);
		cv::threshold(differenceImage,thresholdImage,SENSITIVITY_VALUE,255,THRESH_BINARY_INV);

		cv::absdiff(grayImageB1,grayImageB2,differenceImageB);
		cv::threshold(differenceImageB,thresholdImageB,SENSITIVITY_VALUE,255,THRESH_BINARY);

		cv::blur(thresholdImageB,thresholdImageB,cv::Size(BLUR_SIZE,BLUR_SIZE));
		cv::threshold(thresholdImageB,thresholdImageB,SENSITIVITY_VALUE,255,THRESH_BINARY_INV);

		maskedThresholdImageB = Mat::ones(thresholdImageB.size(), CV_8UC1);
		maskB = Mat::zeros(thresholdImageB.size(), CV_8UC1);
		rectangle( maskB,
           Point( thresholdImageB.size().width * 0.2, thresholdImageB.size().height * 0.2 ),
           Point( thresholdImageB.size().width * 0.8, thresholdImageB.size().height * 0.8),
           Scalar( 255, 255, 255 ), -1, 8);

		thresholdImageB.copyTo(maskedThresholdImageB, maskB);

		imshow("Threshold Top", thresholdImage);
		imshow("Threshold Side", thresholdImageB);
		imshow("Masked Threshold Side", maskedThresholdImageB);

		SimpleBlobDetector detector1(params);
		detector1.detect( thresholdImage, keypointsA);
		drawKeypoints( frame1, keypointsA, im_with_keypointsA, Scalar(0,255,0), DrawMatchesFlags::DRAW_RICH_KEYPOINTS );

		SimpleBlobDetector detector2(params);
		detector1.detect( maskedThresholdImageB, keypointsB);
		drawKeypoints( frameB1, keypointsB, im_with_keypointsB, Scalar(0,255,0), DrawMatchesFlags::DRAW_RICH_KEYPOINTS );

		tableTopPx = thresholdImageB.size().height * 0.515;
		tableHeightPx = thresholdImageB.size().height * (0.8 - 0.515);
		tableWidthPx = thresholdImage.size().height;
		tableLengthPx = thresholdImage.size().width;

    if(keypointsA.size() && keypointsB.size()) {
			ballYPositonCM = (tableTopPx - keypointsB[0].pt.y) * TABLE_HEIGHT_CENTIMETERS / tableHeightPx;
			ballZPositonCM = (tableWidthPx - keypointsA[0].pt.y) * TABLE_WIDTH_CENTIMETERS / tableWidthPx;
			ballXPositonCM = keypointsA[0].pt.x * TABLE_LENGTH_CENTIMETERS / tableLengthPx;

			cout << "======= Ball Position ==============" << endl;
			cout << ballXPositonCM << "cm along the length of the table." << endl;
			cout << ballZPositonCM << "cm width the width of the table." << endl;
			cout << ballYPositonCM << "cm above the table." << endl;

			putText(im_with_keypointsA,"Ball at (" + intToString(ballXPositonCM)+"cm,"+intToString(ballYPositonCM)+"cm,"+intToString(ballZPositonCM)+"cm)",Point(8,18),1,1,Scalar(0,250,0),2);
			putText(im_with_keypointsB,"Ball at (" + intToString(ballXPositonCM)+"cm,"+intToString(ballYPositonCM)+"cm,"+intToString(ballZPositonCM)+"cm)",Point(8,18),1,1,Scalar(0,250,0),2);

		}

		imshow("Final Top",im_with_keypointsA);
		imshow("Final Side",im_with_keypointsB);

		waitKey(30);
	}

	return 0;

}
